<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class training_list extends Model
{
    protected $table = "training_lists";
    protected $primaryKey = 'id';
    protected $fillable = ["id", "id_scholarship", "code", "title_training", "description", "training_schema","participant_scheme","date","available", "link_registration", "link_whatsapp", "price", "batch"];

    public function scholarship()
    {
        // return $this->belongsTo(Category::class);
        return $this->belongsTo(scholarship_list::class, 'id_scholarship');
    }


}
