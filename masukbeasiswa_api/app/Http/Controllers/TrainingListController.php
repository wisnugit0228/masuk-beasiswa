<?php

namespace App\Http\Controllers;

use App\training_list;
use Illuminate\Http\Request;

class TrainingListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $training = training_list::with('scholarship')->get();
        return $training;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $training = new training_list();

        function generateRandomString($param, $length = 2)
        {
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return strtoupper($param[0]). strtoupper($param[strlen($param) - 1]).'' . $randomString;
        }

        $training->id_scholarship = $request->id_scholarship;
        $training->code = generateRandomString($request->title_training);
        $training->title_training = $request->title_training;
        $training->description = $request->description;
        $training->training_schema = $request->training_schema;
        $training->participant_scheme = $request->participant_scheme;
        $training->date = $request->date;
        $training->available = $request->available;
        $training->link_registration = $request->link_registration;
        $training->link_whatsapp = $request->link_whatsapp;
        $training->price = $request->price;
        $training->batch = $request->batch;
        $training->save();
        return $training;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\training_list  $training_list
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $training = training_list::findOrFail($id);
        return $training;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\training_list  $training_list
     * @return \Illuminate\Http\Response
     */
    public function edit(training_list $training_list)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\training_list  $training_list
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $scholarship = training_list::findOrFail($id);
        $scholarship->update($requestData);

        return $scholarship;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\training_list  $training_list
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        training_list::destroy($id);
        return "success";
    }
}
