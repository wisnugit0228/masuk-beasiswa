<?php

namespace App\Http\Controllers;

use App\scholarship_list;
use Illuminate\Http\Request;

class ScholarshipListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scholarship = scholarship_list::all();
        return $scholarship;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $scholarship = new scholarship_list();

        function generateRandomString($param, $length = 2)
        {
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return strtoupper($param[0]). strtoupper($param[strlen($param) - 1]).'' . $randomString;
        }

        $scholarship->code = generateRandomString($request->name);
        $scholarship->name = $request->name;
        $scholarship->save();
        return $scholarship;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\scholarship_list  $scholarship_list
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $scholarship = scholarship_list::findOrFail($id);
        return $scholarship;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\scholarship_list  $scholarship_list
     * @return \Illuminate\Http\Response
     */
    public function edit(scholarship_list $scholarship_list)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\scholarship_list  $scholarship_list
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $scholarship = scholarship_list::findOrFail($id);
        $scholarship->update($requestData);

        return $scholarship;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\scholarship_list  $scholarship_list
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        scholarship_list::destroy($id);
        return "success";
    }
}
