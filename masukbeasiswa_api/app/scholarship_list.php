<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class scholarship_list extends Model
{
    protected $table = "scholarship_lists";
    protected $primaryKey = 'id';
    protected $fillable = ["id", "code", "name"];

    
    public function training()
    {
        return $this->hasMany(training_list::class);
        //Or: return $this->hasMany(Post::class, 'foreign_key');
    }
}
