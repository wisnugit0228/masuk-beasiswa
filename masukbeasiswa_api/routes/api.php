<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () {
    Route::get('scholarship', 'ScholarshipListController@index');
    Route::get('scholarship/{id}', 'ScholarshipListController@show');
    Route::delete('scholarship/{id}', 'ScholarshipListController@destroy')->middleware('jwt.verify');
    Route::post('scholarship', 'ScholarshipListController@store')->middleware('jwt.verify');
    Route::put('scholarship/{id}', 'ScholarshipListController@update')->middleware('jwt.verify');

    Route::get('training-list', 'TrainingListController@index');
    Route::get('training-list/{id}', 'TrainingListController@show');
    Route::delete('training-list/{id}', 'TrainingListController@destroy')->middleware('jwt.verify');
    Route::post('training-list', 'TrainingListController@store')->middleware('jwt.verify');
    Route::put('training-list/{id}', 'TrainingListController@update')->middleware('jwt.verify');
    
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');
    Route::post('change-password', 'UserController@changePassword')->middleware('jwt.verify');
});