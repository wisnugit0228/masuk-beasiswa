<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_scholarship');
            $table->string('code')->nullable();
            $table->string('title_training')->nullable();
            $table->string('description')->nullable();
            $table->string('training_schema')->nullable();
            $table->string('participant_scheme')->nullable();
            $table->string('date')->nullable();
            $table->boolean('available')->nullable();
            $table->string('link_registration')->nullable();
            $table->string('link_whatsapp')->nullable();
            $table->string('price')->nullable();
            $table->string('batch')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_lists');
    }
}
